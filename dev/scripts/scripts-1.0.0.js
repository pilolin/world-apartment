(function($) {
  $(document).ready(function() {

    // мобильное меню
    $('.header__toggle-menu').on('click', function() {
      $(this).toggleClass('on');
      $('.header-navigation').slideToggle(250);
    });

    // кастомизация select для фильтра адресов
    $('#filter-address').select2({
      closeOnSelect: false,
      width: '100%'
    });

    // слайдер на главной по новинкам
    $('#new-offers .offers-slider .offers-slider-wrap').slick({
      infinite: false,
      prevArrow: '#new-offers .offers__control-button--left',
      nextArrow: '#new-offers .offers__control-button--right',
      slidesToShow: 4,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 992,
          settings: { slidesToShow: 3 }
        }, 
        {
          breakpoint: 768,
          settings: { slidesToShow: 2 }
        }, 
        {
          breakpoint: 576,
          settings: { slidesToShow: 1 }
        }
      ]
    });

    // слайдер на главной по продажам
    $('#sell-apartment .offers-slider .offers-slider-wrap').slick({
      infinite: false,
      prevArrow: '#sell-apartment .offers__control-button--left',
      nextArrow: '#sell-apartment .offers__control-button--right',
      slidesToShow: 4,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 992,
          settings: { slidesToShow: 3 }
        }, 
        {
          breakpoint: 768,
          settings: { slidesToShow: 2 }
        }, 
        {
          breakpoint: 576,
          settings: { slidesToShow: 1 }
        }
      ]
    });

    // слайдер на главной по аренде
    $('#rent-apartment .offers-slider .offers-slider-wrap').slick({
      infinite: false,
      prevArrow: '#rent-apartment .offers__control-button--left',
      nextArrow: '#rent-apartment .offers__control-button--right',
      slidesToShow: 4,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 992,
          settings: { slidesToShow: 3 }
        }, 
        {
          breakpoint: 768,
          settings: { slidesToShow: 2 }
        }, 
        {
          breakpoint: 576,
          settings: { slidesToShow: 1 }
        }
      ]
    });

    // слайдер на главной по агентам
    $('#contact-manager .agents-slider .agents-slider-wrap').slick({
      infinite: false,
      prevArrow: '#contact-manager .agents__control-button--left',
      nextArrow: '#contact-manager .agents__control-button--right',
      slidesToShow: 4,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 992,
          settings: { slidesToShow: 3 }
        }, 
        {
          breakpoint: 768,
          settings: { slidesToShow: 2 }
        }, 
        {
          breakpoint: 576,
          settings: { slidesToShow: 1 }
        }
      ]
    }).on('setPosition', function() {
      $('.agents-item__preview img').equalHeight();
      $('.agents-item__preview').equalHeight();
    });

    // слайдер на главной по партнерам
    $('#partners .partners-slider .partners-slider-wrap').slick({
      infinite: false,
      prevArrow: '#partners .partners__control-button--left',
      nextArrow: '#partners .partners__control-button--right',
      slidesToShow: 4,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 992,
          settings: { slidesToShow: 3 }
        }, 
        {
          breakpoint: 576,
          settings: { slidesToShow: 2 }
        }, 
        {
          breakpoint: 380,
          settings: { slidesToShow: 1 }
        }
      ]
    }).on('setPosition', function() {
      $('.partners-slider .slick-slide').height($('.partners-slider .slick-slide').width());
    });

    // ползунок в фильтре для цены
    var inputPriceSlider = document.getElementById('slider-for-price'),
        inputsPrice = {
          0: document.getElementById('filter-price-min'),
          1: document.getElementById('filter-price-max'),
        };

    if( inputPriceSlider ) {
      noUiSlider.create(inputPriceSlider, {
        start: [0, inputPriceSlider.dataset.max],
        connect: true,
        step: +inputPriceSlider.dataset.step,
        range: {
            'min': +inputPriceSlider.dataset.min,
            'max': +inputPriceSlider.dataset.max
        }
      });

      inputPriceSlider.noUiSlider.on('update', function(values, handle) {
        $(this.target).siblings(`[data-handle="${handle}"]`).val(Math.round(values[handle]));
      });

      $.each(inputsPrice, function(i, e) {
        e.addEventListener('change', function () {
          if( +this.dataset.handle == 0 ) {
            inputPriceSlider.noUiSlider.set([this.value, null]);
          } else {
            inputPriceSlider.noUiSlider.set([null, this.value]);
          }
        });
      });
    }

    // ползунок в фильтре для площади
    var inputAreaSlider = document.getElementById('slider-for-area'),
    inputsArea = {
      0: document.getElementById('filter-area-min'),
      1: document.getElementById('filter-area-max'),
    };
    
    if( inputAreaSlider ) {
      noUiSlider.create(inputAreaSlider, {
        start: [0, inputAreaSlider.dataset.max],
        connect: true,
        step: +inputAreaSlider.dataset.step,
        range: {
            'min': +inputAreaSlider.dataset.min,
            'max': +inputAreaSlider.dataset.max
        }
      });

      inputAreaSlider.noUiSlider.on('update', function(values, handle) {
        $(this.target).siblings(`[data-handle="${handle}"]`).val(Math.round(values[handle]));
      });

      $.each(inputsArea, function(i, e) {
        e.addEventListener('change', function () {
          if( +this.dataset.handle == 0 ) {
            inputAreaSlider.noUiSlider.set([this.value, null]);
          } else {
            inputAreaSlider.noUiSlider.set([null, this.value]);
          }
        });
      });
    }

    // слайдер в карточке продукта
    $('.product__main-slider').slick({
      lazyLoad: 'ondemand',
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      swipe: false,
      asNavFor: '.product__nav-slider'
    });

    // слайдер в карточке продукта - навигация
    $('.product__nav-slider').slick({
      slidesToShow: 6,
      slidesToScroll: 1,
      asNavFor: '.product__main-slider',
      focusOnSelect: true,
      responsive: [
        {
          breakpoint: 1199,
          settings: { slidesToShow: 5 }
        }, 
        {
          breakpoint: 992,
          settings: { slidesToShow: 3 }
        }, 
        {
          breakpoint: 768,
          settings: { slidesToShow: 6 }
        }, 
        {
          breakpoint: 568,
          settings: { slidesToShow: 4 }
        }, 
        {
          breakpoint: 420,
          settings: { slidesToShow: 2 }
        }
      ]
    }).on('setPosition', function (event, slick) {
      $('.product__nav-slider .slick-slide').css('height', slick.$slideTrack.height());
    });

    // открытие модальных окон
    $('.link-magnific').magnificPopup({
      type:'inline',
      midClick: true,
      removalDelay: 500,
      callbacks: {
        beforeOpen: function() {
          this.st.mainClass = 'mfp-zoom-in';
        }
      },
      midClick: true
    });

    // анимация label в модальной форме 
    $('.modal-form-item--text input').on('focusin', function() {
      if( $(this).val() != undefined || $(this).val() != '' ) {
        $(this).siblings('label')
        .css({
          'font-size': 12,
          'font-weight': 600,
        })
        .animate({
          top: -5,
          left: 10,
        }, 100);
      }
    });
    
    $('.modal-form-item--text input').on('focusout', function() {
      if( $(this).val() == undefined || $(this).val() == '' ) {
        $(this).siblings('label')
          .css({
            'font-size': 16,
            'font-weight': 400,
          })
          .animate({
            top: 13,
            left: 20,
          }, 100);
        }
    })

    $('.modal-form-item--text input').on('', function() {
      if( $(this).val() != undefined || $(this).val() != '' ) {
        if( !$(this).hasClass('not-empty') ) {
          $(this).addClass('not-empty');
          $(this).siblings('label')
            .css({
              'font-size': 12,
              'font-weight': 600,
            })
            .animate({
              top: -5,
              left: 10,
            }, 100);
        }
      } else {
        if( $(this).hasClass('not-empty') ) {
          $(this).addClass('not-empty');
          $(this).siblings('label')
            .css({
              'font-size': 16,
              'font-weight': 400,
            })
            .animate({
              top: 13,
              left: 20,
            }, 100);
        }
      }
    });
  
  });

  $.fn.equalHeight = function() {
    var tallestcolumn = 0;
    this.height('auto')
    this.each(function() {
      currentHeight = $(this).height();
      if(currentHeight > tallestcolumn) {
        tallestcolumn = currentHeight;
      }
    });
    this.height(tallestcolumn);
  };
}(jQuery))

