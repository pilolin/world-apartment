'use strict';

var gulp = require('gulp'),
	browserSync = require('browser-sync'),
	reload = browserSync.reload,
	sass = require('gulp-sass'),
	cleanCSS = require('gulp-clean-css'),
	rename = require('gulp-rename'),
	sourcemaps = require('gulp-sourcemaps'),
	imagemin = require('gulp-imagemin'),
	pngquant = require('imagemin-pngquant'),
	prefixer = require('gulp-autoprefixer'),
	gulpIf = require('gulp-if'),
	notify = require('gulp-notify'),
	del = require('del'),
	cache = require('gulp-cache'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify');

const isDev = !process.env.NODE_ENV || process.env.NODE_ENV == 'development';

gulp.task('clean', function() {
	return del('app');
});

gulp.task('clearCache', function() {
	return cache.clearAll();
});

gulp.task('html', function() {
	return gulp.src('dev/*.html')
	.pipe(gulp.dest('app'))
	.pipe(reload({ stream: true }));
});

gulp.task('sass', function() {
	return gulp.src('dev/styles/styles.scss')
	.pipe(gulpIf(isDev, sourcemaps.init()))
	.pipe(sass({outputStyle: 'compressed'}))
		.on('error', notify.onError(function (err) {
			return {
				title: 'Styles',
				message: err.message
			}
		}))
	.pipe(prefixer(['last 15 versions', '> 1%'], {cascade: true}))
	.pipe(cleanCSS({ compatibility: 'ie9' }))
	.pipe(rename('styles/styles.min.css'))
	.pipe(gulpIf(isDev, sourcemaps.write()))
	.pipe(gulp.dest('app'))
	.pipe(reload({ stream: true }));
});

gulp.task('img', function() {
	return gulp.src('dev/assets/images/**/*.*')
		.pipe(cache(imagemin({
			interlaced: true,
			progressive: true,
			svgoPlugins: [{ removeViewBox: false}],
			use: [pngquant()]
		})))
		.pipe(gulp.dest('app/assets/images'))
		.pipe(reload({ stream: true }));
});

gulp.task('js', function() {
	return gulp.src('dev/scripts/*.js')
		.pipe(gulpIf(isDev, sourcemaps.init()))
		.pipe(concat('scripts/all-script.js'))
		.pipe(gulpIf(!isDev, uglify()))
		.pipe(gulpIf(isDev, sourcemaps.write()))
		.pipe(gulp.dest('app'))
		.pipe(reload({ stream: true }));
});

gulp.task('server', function() {
	browserSync({
		server: {
			baseDir: 'app'
		},
		notify: false
	});
});

gulp.task('build', ['clean', 'sass', 'js', 'html', 'img']);

gulp.task('watch', ['server', 'sass', 'js', 'html'], function() {
	gulp.watch('dev/styles/*.scss', ['sass'],function(event, cb) {
        setTimeout(function(){gulp.start('sass');}, 50) // задача выполниться через 500 миллисекунд и файл успеет сохраниться на диске
    });
	gulp.watch('dev/scripts/*.js', ['js']);
	gulp.watch('dev/*.html', ['html']);
});

gulp.task('dev', ['build', 'watch']);